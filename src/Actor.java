import java.awt.*;
import java.util.*;

public abstract class Actor {
    ArrayList<Polygon> polygons = new ArrayList<>();;
    Cell loc;

    public void paint(Graphics g){
        
        for(int i=0; i<polygons.size(); i++)
        {
            g.drawPolygon(polygons.get(i));
        }
    }
}