import java.awt.*;
import java.util.*;

class Grid {
    //fields
    Cell[][] cells = new Cell[20][20];

    // constructor
    public Grid(){
        for(int i = 0; i < cells.length; i++){
            for(int j = 0; j < cells[i].length; j++){
                cells[i][j] = new Cell(10+35*i,10+35*j);
            }
        }
    }

    // methods
    public void paint(Graphics g, Point mousePos){
        for(int i = 0; i < cells.length; i++){
            for(int j = 0; j < cells[i].length; j++){
                cells[i][j].paint(g, mousePos);
            }
        }
    }

    public Cell cellAtColRow(int c, int r){
        return cells[c][r];
    }

    public Optional<Cell> cellAtPoint(Point p)
    {   
        Cell cellAtPoint = new Cell(1,1);

        for(int i=0; i<cells.length; i++)
        {
            Cell test = cells[i][0];
            if(test.x >= p.x && test.x < p.x + cells[i][0].size)
            {
                for(int j=0; j<cells[i].length; j++)
                {
                    if(test.y >= p.y && test.y < p.y + cells[i][j].size)
                        cellAtPoint = cells[i][j];
                }
            }
        }

        return Optional.of(cellAtPoint);
    }
}