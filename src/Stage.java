import java.awt.*;
import java.util.*;

public class Stage {
    Grid grid;
    ArrayList<Actor> actors = new ArrayList<>();

    public Stage(){
        grid = new Grid();
        actors.add(new Puppy(grid.cellAtColRow(0, 0)));
        actors.add(new Lion(grid.cellAtColRow(0, 18)));
        actors.add(new Rabbit(grid.cellAtColRow(14,3)));
    }

    public void paint(Graphics g, Point mouseLoc){
        grid.paint(g,mouseLoc);
        for(int i=0; i< actors.size(); i++)
        {    
            Actor temp = actors.get(i);
            temp.paint(g);
        }

        
    }
}